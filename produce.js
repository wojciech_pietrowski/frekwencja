process.on("SIGPIPE", process.exit);

const Jimp = require('jimp');

const loadImage = (path) => {
    return new Promise((resolve, reject) => {
        Jimp.read(path, (err, image) => {
            if (err) {
                reject(err);
            } else {

                resolve(image);
            }
        });
    });
};

const loopImage = (image, times) => {
    return new Promise((resolve, reject) => {
        let loop = 0;
        let frame = 0;
        let interval = setInterval(() => {
            let output = "";
            for (let x = 0; x < 16; x++) {
                for (let y = 0; y < 16; y++) {
                    let X = frame * 16 + x;
                    let Y = x % 2 ? 15 - y : y;
                    let rgba = Jimp.intToRGBA(image.getPixelColor(X, Y));
                    if (rgba.a > 0)
                        output += rgba.r + " " + rgba.g + " " + rgba.b + " ";
                    else
                        output += "0 0 0 ";
                }
            }
            console.log(output);
            frame = (frame + 1) % 8;
            loop++;
            if (loop === times) {
                clearInterval(interval);
                resolve();
            }
        }, 200);
    });
};

const images = [
    "images/Items_Hpot.png",
    "images/Items_Mushroom.png",
    "images/Items_Heart.png",
    "images/Items_Coin.png"
];
let imageIndex = 0;

const next = () => {
    loadImage(images[imageIndex]).then(image => loopImage(image, 16).then(() => next()));
    imageIndex = (imageIndex + 1) % images.length;
};

next();
