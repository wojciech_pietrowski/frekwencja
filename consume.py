import board
import neopixel
import sys

pixels = neopixel.NeoPixel(board.D18, 256, brightness=0.1, auto_write=False)
pixels[0] = (255, 0, 0)
pixels.show()

for line in sys.stdin:

    parts=line.split()

    for x in range(256):
        pixels[x] = (int(parts[x*3]) , int(parts[x*3+1]) , int(parts[x*3+2]))

    pixels.show()
