process.on("SIGPIPE", process.exit);

const Jimp = require('jimp');

const loadImage = (path) => {
    return new Promise((resolve, reject) => {
        Jimp.read(path, (err, image) => {
            if (err) {
                reject(err);
            } else {

                resolve(image);
            }
        });
    });
};

const showImage = (image) => {
    let output = "";
    for (let x = 0; x < 16; x++) {
        for (let y = 0; y < 16; y++) {
            let X = x;
            let Y = x % 2 ? 15 - y : y;
            let rgba = Jimp.intToRGBA(image.getPixelColor(X, Y));
            if (rgba.a > 0)
                output += rgba.r + " " + rgba.g + " " + rgba.b + " ";
            else
                output += "0 0 0 ";
        }
    }
    console.log(output);
};


loadImage("images/heart.png").then(image => showImage(image));
